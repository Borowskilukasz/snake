package com.lukaszborowski;

import com.lukaszborowski.controller.GameController;
import com.lukaszborowski.view.TerminalGameView;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application{

	public static void main(String[] args) {
		

		
		int gameHeight;
		int gameLenght;
//		Scanner sc = new Scanner(System.in);
//		System.out.println("Wybierz opcje uruchomienia gry: ");
//		System.out.println("1. Terminal");
//		System.out.println("2. Okienko");
//        if(sc.nextInt() == 1) {
//        	sc.close();
//        	TerminalGameView terminalView = new TerminalGameView();
//			gameHeight = terminalView.getAreaHeight();
//			gameLenght = terminalView.getAreaLenght();
//			GameController gameController = new GameController(terminalView, gameLenght, gameHeight);
//			gameController.firstScreen();
			
//        }else if(sc.nextInt() == 2)   { 
//        	sc.close();		
		launch(args);
//        }else {
//        	System.out.println("Nie prawidłowe dane\n\n");
//        }
//		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

//		GameController gameController = new GameController(loader.getController(), 800, 500);
//		gameController.firstScreen();
		
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(this.getClass().getResource("/Mainscreen.fxml"));
		StackPane stackPane = loader.load();		
		
		Scene scene = new Scene(stackPane);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Snake");
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	
}
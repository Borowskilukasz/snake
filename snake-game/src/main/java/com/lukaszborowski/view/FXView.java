package com.lukaszborowski.view;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

public class FXView implements Runnable{
	public static final boolean DEBUG_GAME = true;
	public static final boolean DEBUG_SETTINGS_VIEW = false;
	public static final boolean DEBUG_SETTINGS_GAME = false;
	public static final boolean DEBUG_NAME = false;
	
	private int[][] gameOptions = {{0,0},{5,10,200}};
	
	@FXML
	StackPane stackPane;

	private Node node;
	
	private FirstScreenFX firstScreen;


	@FXML
	public void initialize() {

		if(DEBUG_GAME) 	System.out.println("GAME: FXView.initialize main view");
		showFirstScreen("Gosc");
		}

	

	public void clearScreen() {
		stackPane.getChildren().clear();
	}
	
	public void setScreen(Node node) {
		stackPane.getChildren().clear();
		stackPane.getChildren().add(node);
	}

	public int showFirstScreen(String snakeName) {
		if(DEBUG_NAME) 	System.out.println("NAME: FXView snake name:" + snakeName);
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/StartScreen.fxml"));
		Pane Pane = null;
		try {
			Pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		firstScreen = loader.getController();
		firstScreen.setFxview(this);
		firstScreen.setSnakeName(snakeName);
		firstScreen.gameController.setSnakeName(snakeName);
		firstScreen.gameController.setGameOptions(gameOptions);
		firstScreen.newGameBtn.setOnKeyPressed(e-> {
			firstScreen.setChoice(1);
			(new Thread(firstScreen.gameController)).start();
			firstScreen.gameController.game();	
			if(DEBUG_GAME) 	System.out.println("GAME: FXView.showFirstScreen.newGame");		
		});
		firstScreen.changeNameBtn.setOnAction(e-> {
			firstScreen.setChoice(2);
			firstScreen.startt();
		});
		firstScreen.scoretableBtn.setOnAction(e-> {
			firstScreen.setChoice(3);
			firstScreen.startt();
		});
		firstScreen.propertiesBtn.setOnAction(e-> {
			
			firstScreen.setChoice(4);
			firstScreen.startt();
		});
		firstScreen.aboutBtn.setOnAction(e-> {
			
			firstScreen.setChoice(5);
			firstScreen.startt();
		});
		setScreen(Pane);
		return 0;
	}

	
	
	public FirstScreenFX getFirstScreen() {
		return firstScreen;
	}
	
	public int getGameOptions(int x, int y) {
		return gameOptions[x][y];
	}

	public void setGameOptions(int[][] gameOptions) {
		this.gameOptions = gameOptions;
	}

	public Node getNode() {
		return node;
	}
	
	public void setNode(Node node) {
		this.node = node;
	}





	@Override
	public void run() {
		stackPane.getChildren().clear();
		stackPane.getChildren().add(node);
	}
}

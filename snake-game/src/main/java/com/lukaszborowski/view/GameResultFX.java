package com.lukaszborowski.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class GameResultFX {

	@FXML
	Label nameResultLabel;

	@FXML
	Label scoreLabel;

	@FXML
	Button restartBtn;
	
	@FXML
	Button menuBtn;

	@FXML
	Button exitBtn;
	
	
	private String currentName;
	
	private int score;

	private FirstScreenFX firstScreenFX;
	
	private FXView fxview;

	
	public void newGame() {
		firstScreenFX.mainLoader = new FXMLLoader(this.getClass().getResource("/fxml/GameScreen.fxml"));
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: FirstScreenFX.newGame zaczęto metodę");		
		
		firstScreenFX.gc1 = firstScreenFX.canvas.getGraphicsContext2D();
		firstScreenFX.canvas.setFocusTraversable(true);
		fxview.setScreen(firstScreenFX.canvas);
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: FXView.showFirstScreen.newGame ustawiono ekran gry");
		(new Thread(firstScreenFX.gameController)).start();
	}
	
	public void menu() {
		firstScreenFX.setChoice(0);
		fxview.showFirstScreen(currentName);
	}
	
	public void exit() {
		System.exit(0);
	}
	
	
	public Label getScoreLabel() {
		return scoreLabel;
	}

	public void setScoreLabel(String score) {
		scoreLabel.setText(score);
	}
	
	public void setNameResultLabel(String name) {
		nameResultLabel.setText("Brawo " + name);
	}
	
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public String getCurrentName() {
		return currentName;
	}

	public void setCurrentName(String currentName) {
		this.currentName = currentName;
	}

	public FirstScreenFX getFirstScreenFX() {
		return firstScreenFX;
	}

	public void setFirstScreenFX(FirstScreenFX firstScreenFX) {
		this.firstScreenFX = firstScreenFX;
	}

	public FXView getFxview() {
		return fxview;
	}

	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}
	
	
	
}

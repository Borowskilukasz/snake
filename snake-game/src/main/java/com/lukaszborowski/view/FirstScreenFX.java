package com.lukaszborowski.view;

import java.io.IOException;

import com.lukaszborowski.controller.GameController;
import com.sun.prism.impl.BaseMesh.FaceMembers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class FirstScreenFX implements CurrentView {

	private int choice, score;
	private String currentName;
	private String snakeName;
	private int[][] gameOptions= {{0,0},{5,10,200}}; 
	private KeyCode oldkey = null;
	private KeyCode key;
	public Color snakeColor = Color.DARKGREEN;
	public Color appleColor = Color.BROWN;
	public Color borderColor = Color.RED;
	public static final int RESIZE_COL = 10;
	public static final int RESIZE_ROW = 10;
	

	FXMLLoader mainLoader; 
	public final Canvas canvas  = new Canvas(800, 500);
	GameController gameController = new GameController(this, 80, 50);
	private FXView fxview;
	GraphicsContext gc1;

	@FXML
	Button newGameBtn;
	@FXML
	Button changeNameBtn;
	@FXML
	Button scoretableBtn;
	@FXML
	Button propertiesBtn;
	@FXML
	Button aboutBtn;
	
	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}

	public void startt() {
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: FXView.startt");
		if(FXView.DEBUG_NAME) System.out.println("FirstScreenFX.startt");
		gameController.firstScreen();
	}
	public void newGame() {
		mainLoader = new FXMLLoader(this.getClass().getResource("/fxml/GameScreen.fxml"));
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: FirstScreenFX.newGame zaczęto metodę");		
		
		gc1 = canvas.getGraphicsContext2D();
		canvas.setFocusTraversable(true);
		fxview.setScreen(canvas);
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: FXView.showFirstScreen.newGame ustawiono ekran gry");
		(new Thread(gameController)).start();
	}

	@FXML
	public void exit() {
		System.exit(0);
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void drawSnakeElement(int column, int row) {
		Painter.paintPoint(column, row, 10, 10, 10, gc1, snakeColor);
	}

	@Override
	public void drawSnakeHead(int headColumn, int HeadRow) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Painter.paintPoint(headColumn, HeadRow, 10, 10, 10, gc1, snakeColor);
	}

	@Override
	public void drawBorders() {
		
				gc1.clearRect(0, 0, 800, 500);
				Painter.paintLine(0, 0, 10, getAreaHeight(), 1, gc1, borderColor);
				Painter.paintLine(0, 0, getAreaLenght(), 10, 1, gc1, borderColor);
				Painter.paintLine(getAreaLenght()-20, 0, 10, getAreaHeight(), 1, gc1, borderColor);
				Painter.paintLine(0, (getAreaHeight()-20), getAreaLenght(), 10, 1, gc1, borderColor);
				}

	@Override
	public void makeAppleVisible(int column, int row) {
		Painter.paintPoint(column, row, 10, 10, RESIZE_COL, gc1, appleColor);
	}

	@Override
	public int getAreaHeight() {
		return (int) fxview.stackPane.getHeight();
	}

	@Override
	public int getAreaLenght() {
		return (int) fxview.stackPane.getWidth();
	}

	@Override
	public void setKey() {
		canvas.setOnKeyPressed(e -> key = e.getCode());
		System.out.println(key);
	}

	@Override
	public String keyToString() {
		if (key != null && key != oldkey) {
			switch(key) {
			case RIGHT:
				oldkey = key;
				return "ArrowRight";
			case LEFT:
				oldkey = key;
				return "ArrowLeft";
			case UP:
				oldkey = key;
				return "ArrowUp";
			case DOWN:
				oldkey = key;
				return "ArrowDown";
			case SPACE:
				oldkey = key;
				return "NormalKey:  ";
			case ESCAPE:
				oldkey = key;
				return "Escape";
				default:
					oldkey = key;
					return "";
			}
		} else {
			return "";
		}
	}

	@Override
	public void refreshScreen() {
		//w graficznym interfejsie nie ma potrzeby odświezania ekranu
	}

	@Override
	public int showFirstScreen(String snakeName) {
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: FirstScreenFX.showFirstScreen");
		if(FXView.DEBUG_NAME) System.out.println("FirstScreenFX.showFirstScreen->choice " + choice);
		return choice;
	}

	@Override
	public int showGameResult(int score, String snakeName) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/GameResultScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		GameResultFX gameResultFX = loader.getController();
		gameResultFX.setCurrentName(snakeName);
		gameResultFX.setFirstScreenFX(this);
		gameResultFX.setFxview(fxview);
		gameResultFX.setScoreLabel(Integer.toString(score));
		gameResultFX.setScore(score);
		
		fxview.setNode(pane);
		Platform.runLater(fxview);
		return 0;
	}

	@Override
	public int showPauseView(int score, String snakeName) {
		
		System.out.println("PAUSE");
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/PauseScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		PauseFX pauseFx = loader.getController();
		pauseFx.setFirstScreenFX(this);
		pauseFx.setFxview(fxview);
		pauseFx.setScore(score);
		pauseFx.setScoreLabel(score);
		pauseFx.scoreLabel.setText(Integer.toString(score));
		pauseFx.resumeBtn.setOnAction(e -> {
			synchronized(gameController) {
				gc1 = canvas.getGraphicsContext2D();
				fxview.setScreen(canvas);
				gameController.notify();
			}
		});
		pauseFx.menuBtn.setOnAction( e -> {
			pauseFx.menu();
		});
		
		fxview.setNode(pane);
		Platform.runLater(fxview);
		
		try {
			synchronized(gameController) {
			gameController.wait();
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		return 0;
	}

	@Override
	public String changeSnakeNameScreen(String currentName) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/NameScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ChangeNameFX changeNameFX = loader.getController();
		changeNameFX.setFirstScreenFX(this);
		changeNameFX.setFxview(fxview);
		changeNameFX.setCurrentName(currentName);
		changeNameFX.setPlayerName(currentName);
		fxview.setScreen(pane);
		changeNameFX.applyBtn.setOnAction(e -> {
			if(FXView.DEBUG_NAME) {
				System.out.println("NAME: FirstScreenFX snakeName w lambdzie sprawdzam czy cos daje: " + changeNameFX.newName.getText());
			}
			gameController.setSnakeName(changeNameFX.newName.getText());
			if(FXView.DEBUG_NAME) {
				System.out.println("NAME: FirstScreenFX snakeName w lambdzie sprawdzam czy cos daje: zmieniło? " + gameController.getSnakeName());
			}
			changeNameFX.setCurrentName(changeNameFX.newName.getText());
			changeNameFX.apply();
				});
		
		if(FXView.DEBUG_NAME) System.out.println("NAME: FirstScreenFx.ChangeSnakeNameScreen: przekazywany " + currentName + " zwracany: " + snakeName);
		return snakeName;
	}

	@Override
	public void showBestResultsView(String[] names, int[] results) {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/showScoreboardScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ScoreboardFX scoreboardFX = loader.getController();
		scoreboardFX.setFirstScreenFX(this);
		scoreboardFX.setFxview(fxview);
		scoreboardFX.addScoreToTable(names, results);
		fxview.setScreen(pane);
	}

	@Override
	public int[][] showSettingList(int[][] gameOptions) {

		if(FXView.DEBUG_SETTINGS_GAME) System.out.println("SETTINGS2: FirstScreenFX.showSettingList przekazany paramety:" + gameOptions[0][1]);
		if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS: FirstScreenFX.showSettingList przekazany paramety:"
														+ gameOptions[0][0]);
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/propertiesFirstScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		PropertiesFX propertiesFX = loader.getController();
		propertiesFX.setFirstScreenFX(this);
		propertiesFX.setFxview(fxview);
		propertiesFX.setGameOptions(gameOptions);
		propertiesFX.gameSettingsBtn.setOnAction(e -> {
			propertiesFX.gameSettings();
		});
		propertiesFX.viewSettingsBtn.setOnAction(e -> {
			propertiesFX.viewSettings();
		});
		propertiesFX.backBtn.setOnAction(e -> {
			this.gameOptions = propertiesFX.getGameOptions();
			gameController.setGameOptions(this.gameOptions);
			fxview.setGameOptions(this.gameOptions);
			if(FXView.DEBUG_SETTINGS_GAME) System.out.println("SETTINGS2: FirstScreenFX.showSettingList przekazany paramety:zmiana?" + gameOptions[0][1] 
					+ " opcje z game controllera: " 
					+ gameController.getGameOptions(0,0));
			if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS: FirstScreenFX.showSettingList przekazany paramety:zmiana?"
					+ gameOptions[0][0] 
						+ " opcje z game controllera: " 
							+ gameController.getGameOptions(0,0));
			propertiesFX.back();
		});
		
		fxview.setScreen(pane);
		return null;
	}

	@Override
	public void showAbout() {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/AboutScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		AboutFx aboutFx = loader.getController();
		aboutFx.setFirstScreenFX(this);
		aboutFx.setFxview(fxview);
		fxview.setScreen(pane);

	}

///////////////////////////////////////////////////////////////////////////////////////////
//Getters and setters
//////////////////////////////////////////////////////////////////////////////////////////////////

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getCurrentName() {
		return currentName;
	}

	public void setCurrentName(String currentName) {
		this.currentName = currentName;
	}

	public String getSnakeName() {
		return snakeName;
	}

	public void setSnakeName(String snakeName) {
		this.snakeName = snakeName;
	}

	public int[][] getGameOptions() {
		return gameOptions;
	}

	public void setGameOptions(int[][] gameOptions) {
		this.gameOptions = gameOptions;
	}

	public int getChoice() {
		return choice;
	}

	public void setChoice(int choice) {
		this.choice = choice;
	}
}

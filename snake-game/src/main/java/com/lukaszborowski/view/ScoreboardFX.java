package com.lukaszborowski.view;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class ScoreboardFX {

	@FXML
	TableView<Results> table;
	
	@FXML
	TableColumn<Results, String> lp;
	
	@FXML
	TableColumn<Results, String> name;
	
	@FXML
	TableColumn<Results, String> score;
	
	private FirstScreenFX firstScreenFX;
	private FXView fxview;
	
	
	ObservableList<Results> scorelist = FXCollections.observableArrayList();

	public void addScoreToTable(String[] names,  int[] results) {
		for(int i = 0 ; i< names.length ; i++) {
			int lp = i+1;
			scorelist.add(new Results(lp, names[i], results[i]));
		}
		lp.setCellValueFactory(cellData -> cellData.getValue().getLpg());
		name.setCellValueFactory(cellData -> cellData.getValue().getName());
		score.setCellValueFactory(cellData -> cellData.getValue().getScore());
		table.setItems(scorelist);
	}
	
	public static class Results {
		 
	    private StringProperty lpg = new SimpleStringProperty();
		private StringProperty name = new SimpleStringProperty();
	    private StringProperty score = new SimpleStringProperty();
	    
	    private Results(int lpg, String name, int score) {
	    	this.lpg.set(String.valueOf(lpg));
	    	this.name.set(name);
	    	this.score.set(String.valueOf(score));
	    }

	    public StringProperty getLpg() {
				return lpg;
			}

			public void setLpg(StringProperty lpg) {
				this.lpg = lpg;
			}

			public StringProperty getName() {
				return name;
			}

			public void setName(StringProperty name) {
				this.name = name;
			}

			public StringProperty getScore() {
				return score;
			}

			public void setScore(StringProperty score) {
				this.score = score;
			}
	}

	@FXML
	public void back() {
		firstScreenFX.setChoice(0);
		fxview.showFirstScreen(firstScreenFX.getSnakeName());
	}
	
	
	
	
	

	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}
	
	public void setFirstScreenFX(FirstScreenFX firstScreenFX) {
		this.firstScreenFX = firstScreenFX;
	}
	
}



package com.lukaszborowski.view;

import javafx.fxml.FXML;

public class AboutFx {

	private FirstScreenFX firstScreenFX;

	
	public void setFirstScreenFX(FirstScreenFX firstScreenFX) {
		this.firstScreenFX = firstScreenFX;
	}
	
	private FXView fxview;
	
	
	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}


	@FXML
	public void back() {
		firstScreenFX.setChoice(0);
		fxview.showFirstScreen(firstScreenFX.getSnakeName());
	}
	
}

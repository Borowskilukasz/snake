package com.lukaszborowski.view;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public class PropertiesFX {
	
	@FXML
	Button gameSettingsBtn;
	
	@FXML
	Button viewSettingsBtn;
	
	@FXML
	Button backBtn;
	
	private FirstScreenFX firstScreenFX;
	private FXView fxview;
	private int[][] gameOptions;
	
	
	



	@FXML
	public void viewSettings() {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/propertiesViewScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		PropertiesViewFX propertiesViewFX = loader.getController();
		propertiesViewFX.setFxview(fxview);
		propertiesViewFX.setGameOptions(gameOptions);
		propertiesViewFX.setRadiobuttons();
		propertiesViewFX.applyViewBtn.setOnAction(e ->{		
		propertiesViewFX.selectViewMode();
		gameOptions = propertiesViewFX.getGameOptions();
		if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS: FirstScreenFX.showSettingList opcje po modyfikacjach: "
				+ gameOptions[0][0]);
		propertiesViewFX.back();
				});
		fxview.setScreen(pane);
	}
	
	@FXML
	public void gameSettings() {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/propertiesGameScreen.fxml"));
		Pane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		PropertiesGameFX propertiesGameFX = loader.getController();
		propertiesGameFX.setFxview(fxview);
		propertiesGameFX.setGameOptions(gameOptions);
		propertiesGameFX.setRadioButtons();
		propertiesGameFX.applyGameBtn.setOnAction(e ->{	
			propertiesGameFX.selectGameMode();
			gameOptions = propertiesGameFX.getGameOptions();
			if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS: FirstScreenFX.showSettingList opcje po modyfikacjach: "
					+ gameOptions[0][1]);
		propertiesGameFX.back();
				});
		fxview.setScreen(pane);
	}
	@FXML
	public void back() {
		fxview.showFirstScreen(firstScreenFX.getSnakeName());
	}

	
	
	
	
	
	public int[][] getGameOptions() {
		return gameOptions;
	}

	public void setGameOptions(int[][] gameOptions) {
		this.gameOptions = gameOptions;
	}

	public void setFirstScreenFX(FirstScreenFX firstScreenFX) {
		this.firstScreenFX = firstScreenFX;
	}
	
	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}
}

package com.lukaszborowski.view;


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;

public class PropertiesViewFX {
	
	private FXView fxview;
	private int[][] gameOptions;
	@FXML
	private RadioButton clasicColors;	
	@FXML
	private RadioButton HippieColors;	
	@FXML
	private RadioButton pacmanColors;
	
	@FXML
	Button applyViewBtn;
	
	
	@FXML
	public void back() {
		fxview.getFirstScreen().showSettingList(gameOptions);
	}
	
	public void setRadiobuttons() {
		if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS: PropertiesViewFX.setRadiobuttons opcje: "
				+ gameOptions[0][0]);
		if(gameOptions[0][0] == 0) {
			clasicColors.setSelected(true);
		}else if(gameOptions[0][0] == 1) {
			HippieColors.setSelected(true);
		}else if(gameOptions[0][0] == 2) {
			pacmanColors.setSelected(true);
		}else {
			clasicColors.setSelected(true);
		}
	}
	public void selectViewMode() {
		if(clasicColors.isSelected()) {
			gameOptions[0][0] = 0;
//			setSnakeColor(Terminal.Color.GREEN);
//			setBackgroundColor(Terminal.Color.BLACK);
//			setBorderColor(Terminal.Color.WHITE);
//			setAppleColor(Terminal.Color.RED);
//			hippiestyle = false;
//			
		}else if(HippieColors.isSelected()) {
				gameOptions[0][0] = 1;
//				setSnakeColor(Terminal.Color.GREEN);
//				setBackgroundColor(Terminal.Color.BLACK);
//				setBorderColor(Terminal.Color.RED);
//				setAppleColor(Terminal.Color.RED);
//				hippiestyle = true;
//				
			}else if(pacmanColors.isSelected()) {
				gameOptions[0][0] = 2;
//				setSnakeColor(Terminal.Color.YELLOW);
//				setBackgroundColor(Terminal.Color.BLACK);
//				setBorderColor(Terminal.Color.BLUE);
//				setAppleColor(Terminal.Color.YELLOW);
//				hippiestyle = false;
			
			}
	}
	
	
	
	public int[][] getGameOptions() {
		return gameOptions;
	}

	public void setGameOptions(int[][] gameOptions) {
		this.gameOptions = gameOptions;
	}
	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}
}

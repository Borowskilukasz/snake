package com.lukaszborowski.view;



import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Painter {

	public static void paint(GraphicsContext context) {
		
	}
	
	public static void paintLine(int row, int column, int x, int y, int resizexy, GraphicsContext context, Color color) {
		context.setFill(color);
		context.fillRect(row * resizexy, column * resizexy, x, y);
	}
	
	public static void paintPoint(int row, int column, int x, int y, int resizexy, GraphicsContext context, Color color) {
		context.setFill(color);
		context.fillOval(row * resizexy, column * resizexy, x, y);
	}
}

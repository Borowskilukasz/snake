package com.lukaszborowski.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class PauseFX {

	@FXML
	Button resumeBtn;
	
	@FXML
	Button restartBtn;
	
	@FXML
	Button menuBtn;
	
	@FXML 
	Button exitBtn;
	
	@FXML
	Label scoreLabel;

	private String currentName;
	
	private FirstScreenFX firstScreenFX;
	
	private FXView fxview;
	
	private int Score;
	
	public int getScore() {
		return Score;
	}

	public void setScore(int score) {
		Score = score;
	}
	
	@FXML 
	public void newGame() {
		firstScreenFX.newGame();
	}
	
	@FXML
	public void exit() {
		System.exit(0);
	}
	
	@FXML
	public void menu() {
		firstScreenFX.setChoice(0);
		fxview.showFirstScreen(currentName);
	}
	
	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}


public void setFirstScreenFX(FirstScreenFX firstScreenFX) {
	this.firstScreenFX = firstScreenFX;
}

public String getCurrentName() {
	return currentName;
}

public void setCurrentName(String currentName) {
	this.currentName = currentName;
}

public void setScoreLabel(int score) {
	scoreLabel.setText(Integer.toString(score));
}

}

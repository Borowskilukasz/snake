package com.lukaszborowski.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ChangeNameFX {
	
	@FXML
	TextField newName;
	
	@FXML
	Label playerName;

	@FXML
	Button applyBtn;
	
	@FXML
	Button backBtn;


	
	private String currentName;
	
	private FirstScreenFX firstScreenFX;
	
	private FXView fxview;
	
	
	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}

	@FXML
	public void apply() {
		if(FXView.DEBUG_NAME) {
		System.out.println("NAME: ChangeNameFX.apply currentName: " + currentName);
	}
		firstScreenFX.setChoice(0);
		currentName = newName.getText();
		firstScreenFX.gameController.setSnakeName(currentName);
		fxview.showFirstScreen(currentName);
	}
	@FXML
	public void back() {
		firstScreenFX.setChoice(0);
		fxview.showFirstScreen(currentName);
	}
	
	public void setPlayerName(String playerName) {
		this.playerName.textProperty().set(playerName);
	}
	
	public String getCurrentName() {
		return currentName;
	}

	public void setCurrentName(String currentName) {
		this.currentName = currentName;
	}
	
	
	public FirstScreenFX getFirstScreenFX() {
		return firstScreenFX;
	}
	public void setFirstScreenFX(FirstScreenFX firstScreenFX) {
		this.firstScreenFX = firstScreenFX;
	}
}

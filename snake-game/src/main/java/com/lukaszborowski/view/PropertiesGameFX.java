package com.lukaszborowski.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;

public class PropertiesGameFX {

	private FXView fxview;
	private int[][] gameOptions;
	
	@FXML
	private RadioButton gameModeEasy;	
	@FXML
	private RadioButton gameModeMedium;	
	@FXML
	private RadioButton gameModeHard;	
	@FXML
	private RadioButton gameModeExtreme;
	@FXML 
	Button applyGameBtn;


	@FXML
	public void back() {
		fxview.getFirstScreen().showSettingList(gameOptions);
	}
	
	public void selectGameMode() {
		if(gameModeEasy.isSelected()) {
			gameOptions[0][1] = 0;
			gameOptions[1][0] = 10;
			gameOptions[1][1] = 5;
			gameOptions[1][2] = 200;
			
		}else if(gameModeMedium.isSelected()) {
			gameOptions[0][1] = 1;
			gameOptions[1][0] = 5;
			gameOptions[1][1] = 10;
			gameOptions[1][2] = 170;
				
			}else if(gameModeHard.isSelected()) {
				gameOptions[0][1] = 2;
				gameOptions[1][0] = 3;
				gameOptions[1][1] = 10;
				gameOptions[1][2] = 140;
			
			}else if(gameModeExtreme.isSelected()) {
				gameOptions[0][1] = 3;
				gameOptions[1][0] = 1;
				gameOptions[1][1] = 23;
				gameOptions[1][2] = 110;
			
			}
	}
	
	public void setRadioButtons() {
		if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS2: PropertiesViewFX.setRadiobuttons opcje: "
				+ gameOptions[0][1]);
		if(gameOptions[0][1] == 0) {
			gameModeEasy.setSelected(true);
		}else if(gameOptions[0][1] == 1) {
			gameModeMedium.setSelected(true);
		}else if(gameOptions[0][1] == 2) {
			gameModeHard.setSelected(true);
		}else if(gameOptions[0][1] == 3) {
			gameModeExtreme.setSelected(true);
		}else {
			gameModeEasy.setSelected(true);
		}
	}
	
	public int[][] getGameOptions() {
		return gameOptions;
	}

	public void setGameOptions(int[][] gameOptions) {
		this.gameOptions = gameOptions;
	}

	public void setFxview(FXView fxview) {
		this.fxview = fxview;
	}
	
	
	public Button getApplyGameBtn() {
		return applyGameBtn;
	}

	public void setApplyGameBtn(Button applyGameBtn) {
		this.applyGameBtn = applyGameBtn;
	}

}

package com.lukaszborowski.view;

import java.util.Random;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Component;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.Component.Alignment;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.EmptySpace;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.Table;
import com.googlecode.lanterna.gui.component.TextBox;
import com.googlecode.lanterna.gui.component.Panel.Orientation;
import com.googlecode.lanterna.gui.component.RadioCheckBoxList;
import com.googlecode.lanterna.gui.layout.LinearLayout;
import com.googlecode.lanterna.input.Key;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalSize;
import com.googlecode.lanterna.terminal.Terminal.Color;

/**
 * 
 * @author Lukasz
 *
 */
public class TerminalGameView implements CurrentView {

	// ----------------------------------------------------------------------------------------------
	// private fields
	// ------------------------------------------------------------------------------------------------

	private Color snakeColor = Terminal.Color.GREEN;

	private String snakeBodyLook = "O";
	private String snakeHeadLook = "☺";
	private String appleLook = "ó";
	private String border = " ";
	private Color borderColor = Terminal.Color.WHITE;
	private Color appleColor = Terminal.Color.RED;
	private Color backgroundColor = Terminal.Color.BLACK;
	private boolean hippiestyle = false;

	private Key key = null;
	int choice = 0;
	String SnakeName = "";
	private Terminal terminal;
	private Screen gameScreen;
	final GUIScreen guiScreen;

	// ----------------------------------------------------------------------------------------------
	// constructor
	// ------------------------------------------------------------------------------------------------

	public TerminalGameView() {
		terminal = TerminalFacade.createTerminal();
		guiScreen = TerminalFacade.createGUIScreen(terminal);
		gameScreen = guiScreen.getScreen();
		gameScreen.startScreen();
		gameScreen.setCursorPosition(null);
		gameScreen.refresh();
	}

	// ----------------------------------------------------------------------------------------------
	// methods
	// ------------------------------------------------------------------------------------------------

	public void drawSnakeElement(int column, int row) {
		if(hippiestyle) {
			Random r = new Random();
			int a = r.nextInt(7) + 1;
			Color randomBodyColor = Terminal.Color.GREEN;
			switch(a) {
			case 1: 
				randomBodyColor = Terminal.Color.WHITE;
				break;
			case 2: 
				randomBodyColor = Terminal.Color.BLUE;
				break;
			case 3: 
				randomBodyColor = Terminal.Color.CYAN;
				break;
			case 4: 
				randomBodyColor = Terminal.Color.GREEN;
				break;
			case 5: 
				randomBodyColor = Terminal.Color.MAGENTA;
				break;
			case 6: 
				randomBodyColor = Terminal.Color.RED;
				break;
			case 7: 
				randomBodyColor = Terminal.Color.YELLOW;
				break;
			}
			gameScreen.putString(column, row, snakeBodyLook, randomBodyColor, backgroundColor);			
		}else {
		gameScreen.putString(column, row, snakeBodyLook, snakeColor, backgroundColor);
		}
	}

	public void drawSnakeHead(int headColumn, int HeadRow) {
		gameScreen.putString(headColumn, HeadRow, snakeHeadLook, snakeColor, backgroundColor);
	}

	public void drawBorders() {
		gameScreen.clear();
		for(int col = 0; col < getAreaLenght(); col++) {
			for(int row = 0; row < getAreaHeight(); row++) {
				gameScreen.putString(col, row, border, backgroundColor, backgroundColor);
			}
		}
		// ustawienie krawÄ™dzi
		for (int column = 0; column < getAreaLenght(); column++) {
			// krawedz gorna
			gameScreen.putString(column, 0, border, borderColor, borderColor);
			// krawedz dolna
			gameScreen.putString(column, getAreaHeight() - 1, border, borderColor, borderColor);
			// krawedz lewa
			for (int row = 0; row < getAreaHeight(); row++) {
				gameScreen.putString(0, row, border, borderColor, borderColor);
				// krawedz prawa
				gameScreen.putString(getAreaLenght() - 1, row, border, borderColor, borderColor);
			}
		}
	
	}

	/*
	 * metoda rysuje jablko w konsoli
	 */
	public void makeAppleVisible(int column, int row) {
		gameScreen.putString(column, row, appleLook, appleColor, backgroundColor);
	}

	/**
	 * 
	 * @return wysokosc planszy
	 */
	public int getAreaHeight() {
		return gameScreen.getTerminalSize().getRows();
	}

	/**
	 * 
	 * @return dlugosc planszy
	 */
	public int getAreaLenght() {
		return gameScreen.getTerminalSize().getColumns();
	}

	public String keyToString() {
		if (getKey() != null) {
			return getKey().toString();
		} else {
			return "";
		}
	}

	public void refreshScreen() {
		this.gameScreen.refresh();
	}

	// ---------------------------------------------------------------------------------------------------------------------------------------------------
	// metody obslugujace wyswietlanie okien
	// ---------------------------------------------------------------------------------------------------------------------------------------------------

	public int showFirstScreen(String snakeName) {
		choice = 0;
		if (snakeName.equals("Gość")) {
			snakeName = "Gościu";
		}
		final Window window = new Window("Snake");
		window.setWindowSizeOverride(new TerminalSize(100, 50));
		window.addComponent(new Label("                              #######  ##     #  #######  #   #  ######"));
		window.addComponent(new Label("                              #        # #    #  #     #  #  #   #"));
		window.addComponent(new Label("                              #        #  #   #  #     #  # #    #"));
		window.addComponent(new Label("                              #######  #   #  #  #######  ##     ####"));
		window.addComponent(new Label("                                    #  #    # #  #     #  # #    #"));
		window.addComponent(new Label("                                    #  #     ##  #     #  #  #   #"));
		window.addComponent(new Label("                              #######  #      #  #     #  #   #  ######"));
		window.addComponent(new EmptySpace());
		window.addComponent(new Label("                                        Witaj " + snakeName + "! "));
		window.addComponent(new EmptySpace());
		Button newGameBtn = new Button("Nowa Gra", new Action() {

			public void doAction() {
				choice = 1;
				window.close();
			}
		});
		Button changeNameBtn = new Button("Zmień nazwę użytkownika", new Action() {

			public void doAction() {
				choice = 2;
				window.close();
			}
		});
		Button showScoreTableBtn = new Button("Pokaż tabelę wyników", new Action() {

			public void doAction() {
				choice = 3;
				window.close();
			}
		});
		Button settingsBtn = new Button("Ustawienia", new Action() {

			public void doAction() {
				choice = 4;
				window.close();
			}
		});
		Button aboutBtn = new Button("O grze", new Action() {

			public void doAction() {
				choice = 5;
				window.close();
			}
		});
		Button exitBtn = new Button("Wyjście", new Action() {

			public void doAction() {
				window.close();
				System.exit(0);
			}
		});
		newGameBtn.setAlignment(Alignment.TOP_CENTER);
		window.addComponent(newGameBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(changeNameBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(showScoreTableBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(settingsBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(aboutBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(exitBtn, LinearLayout.GROWS_HORIZONTALLY);
		guiScreen.showWindow(window);

		return choice;
	}

	public String changeSnakeNameScreen(String currentName) {
		SnakeName = currentName;

		final Window windowSnakeName = new Window("Zmień nazwę użytkownika");
		windowSnakeName.setWindowSizeOverride(new TerminalSize(100, 50));
		Label currentNameLabel = new Label("Twoja aktualna nazwa użytkownika to : " + SnakeName);
		TextBox newName = new TextBox(SnakeName);
		Button applyBtn = new Button("Zatwierdz", new Action() {

			public void doAction() {
				SnakeName = newName.getText();
				windowSnakeName.close();
			}
		});
		Button backBtn = new Button("Powrót", new Action() {

			public void doAction() {
				windowSnakeName.close();
			}
		});
		currentNameLabel.setAlignment(Alignment.TOP_CENTER);

		windowSnakeName.addComponent(currentNameLabel, LinearLayout.GROWS_HORIZONTALLY);
		windowSnakeName.addComponent(new EmptySpace());
		windowSnakeName.addComponent(new EmptySpace());
		windowSnakeName.addComponent(new EmptySpace());
		windowSnakeName.addComponent(newName, LinearLayout.GROWS_HORIZONTALLY);
		windowSnakeName.addComponent(new EmptySpace());
		windowSnakeName.addComponent(applyBtn, LinearLayout.GROWS_HORIZONTALLY);
		windowSnakeName.addComponent(backBtn, LinearLayout.GROWS_HORIZONTALLY);
		guiScreen.showWindow(windowSnakeName);
		return SnakeName;
	}

	public void showBestResultsView(String[] names, int[] results) {
		final Window showBestResultsWindow = new Window("");
		showBestResultsWindow.setWindowSizeOverride(new TerminalSize(100, 50));
		Panel tablePanel = new Panel("Najlepsze Wyniki", Orientation.VERTICAL);
		Table table = new Table(10);
		Component[] resultRow = new Component[2];
		for (int i = 0; i < names.length; i++) {
			if (names[i] != null) {
				String name = "";
				for (int j = 0; j < 75 - names[i].length(); j++)
					name += "-";
				resultRow[0] = new Label(names[i] + name);
				resultRow[1] = new Label(Integer.toString(results[i]));
				table.addRow(resultRow);
			} else {
				resultRow[0] = new Label("-----------");
				resultRow[1] = new Label("-----");
				table.addRow(resultRow);
			}
		}
		tablePanel.addComponent(table);
		
		Button backBtn = new Button("Powrót", new Action() {

			public void doAction() {
				showBestResultsWindow.close();
			}
		});
		tablePanel.setAlignment(Component.Alignment.CENTER);
		showBestResultsWindow.addComponent(tablePanel);
		showBestResultsWindow.addComponent(new EmptySpace());
		showBestResultsWindow.addComponent(backBtn, LinearLayout.GROWS_HORIZONTALLY);

		guiScreen.showWindow(showBestResultsWindow);
	}

	public int showGameResult(int score, String snakeName) {
		choice = 0;

		Label l1 = new Label("Brawo " + snakeName + ". Twój wynik to: ");
		Label l2 = new Label(Integer.toString(score));
		l1.setAlignment(Alignment.TOP_CENTER);
		l2.setAlignment(Alignment.TOP_CENTER);
		final Window window = new Window("Koniec Gry!");
		window.setWindowSizeOverride(new TerminalSize(50, 25));
		window.addComponent(new EmptySpace());
		window.addComponent(l1, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l2, LinearLayout.GROWS_HORIZONTALLY);
		Button exitBtn = new Button("Exit", new Action() {

			public void doAction() {
				window.close();
				System.exit(0);
			}
		});
		Button menuBtn = new Button("Menu Główne", new Action() {

			public void doAction() {
				window.close();
			}
		});
		Button restartBtn = new Button("Restart game", new Action() {
			public void doAction() {
				choice = 1;
				window.close();

			}
		});
		restartBtn.setAlignment(Alignment.BOTTON_CENTER);
		window.addComponent(restartBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(menuBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(exitBtn, LinearLayout.GROWS_HORIZONTALLY);
		guiScreen.showWindow(window);
		return choice;

	}

	public int showPauseView(int score, String snakeName) {
		choice = 0;
		Label l1 = new Label("Pauza");
		l1.setAlignment(Alignment.TOP_CENTER);
		final Window window = new Window("Pauza");
		window.setWindowSizeOverride(new TerminalSize(50, 25));
		window.addComponent(new EmptySpace());
		window.addComponent(l1, LinearLayout.GROWS_HORIZONTALLY);
		Button backBtn = new Button("Powrót", new Action() {

			public void doAction() {
				choice = 0;
				window.close();
			}
		});

		Button restartBtn = new Button("Restart game", new Action() {
			public void doAction() {
				choice = 1;
				window.close();

			}
		});
		Button menuBtn = new Button("Menu Główne", new Action() {

			public void doAction() {
				choice = 2;
				window.close();
			}
		});
		Button exitBtn = new Button("Exit", new Action() {

			public void doAction() {
				window.close();
				System.exit(0);
			}
		});

		restartBtn.setAlignment(Alignment.BOTTON_CENTER);
		window.addComponent(backBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(restartBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(menuBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(exitBtn, LinearLayout.GROWS_HORIZONTALLY);
		guiScreen.showWindow(window);
		return choice;

	}

	public void showAbout() {
		final Window window = new Window("O grze");
		window.setWindowSizeOverride(new TerminalSize(100, 50));
		Label l1 = new Label("O grze:");
		Label l2 = new Label("Jest to klasyczna gra snake,");
		Label l3 = new Label("poruszamy się wężem po wyznaczonej planszy");
		Label l4 = new Label("i staramy się zebrac jak najwięcej jabłek");
		Label l5 = new Label("które wąż zjada spotykając na swojej drodze.");	
		Label l6 = new Label("Sterowanie:");
		Label l7 = new Label("Do góry: strzałka w góra,");
		Label l8 = new Label("Do dołu: strzałka w dół,");
		Label l9 = new Label("W lewą: strzałka w lewą,");
		Label l10 = new Label("w prawą: strzałka w prawą,");
		Label l11 = new Label("Pauza: spacja lub escape.");
		Label l12 = new Label("Wykonanie:");
		Label l13 = new Label("Łukasz Borowski.");
		l1.setAlignment(Alignment.TOP_CENTER);


		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(l1, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l2, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l3, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l4, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l5, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(l6, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l7, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l8, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l9, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l10, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l11, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(l12, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(l13, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(new EmptySpace());
		Button backBtn = new Button("Powrót", new Action() {

			public void doAction() {
				window.close();
			}
		});
		
		window.addComponent(backBtn, LinearLayout.GROWS_HORIZONTALLY);
		guiScreen.showWindow(window);
	}
	
	public int[][] showSettingList(int[][] gameOptions) {
		int[][] options = gameOptions;
		final Window window = new Window("Ustawienia");
		window.setWindowSizeOverride(new TerminalSize(100, 50));
		Button backBtn = new Button("Powrót", new Action() {

			public void doAction() {
				window.close();
			}
		});

		Button viewSettingsBtn = new Button("Ustawienia kolorów gry", new Action() {
			public void doAction() {
				Window windowView = new Window("Wygląd");				
				windowView.setWindowSizeOverride(new TerminalSize(100, 50));
				RadioCheckBoxList actionListBox = new RadioCheckBoxList();
				
				actionListBox.addItem("Clasic");
				actionListBox.addItem("Hippie");
				actionListBox.addItem("Pacman");
				actionListBox.setCheckedItemIndex(options[0][0]);
				
				Button back1Btn = new Button("Powrót", new Action() {

					public void doAction() {
						if(actionListBox.getCheckedItem().equals("Clasic")) {
							options[0][0] = 0;
							setSnakeColor(Terminal.Color.GREEN);
							setBackgroundColor(Terminal.Color.BLACK);
							setBorderColor(Terminal.Color.WHITE);
							setAppleColor(Terminal.Color.RED);
							hippiestyle = false;
							
						}else if(actionListBox.getCheckedItem().equals("Hippie")) {
								options[0][0] = 1;
								setSnakeColor(Terminal.Color.GREEN);
								setBackgroundColor(Terminal.Color.BLACK);
								setBorderColor(Terminal.Color.RED);
								setAppleColor(Terminal.Color.RED);
								hippiestyle = true;
								
							}else if(actionListBox.getCheckedItem().equals("Pacman")) {
								options[0][0] = 2;
								setSnakeColor(Terminal.Color.YELLOW);
								setBackgroundColor(Terminal.Color.BLACK);
								setBorderColor(Terminal.Color.BLUE);
								setAppleColor(Terminal.Color.YELLOW);
								hippiestyle = false;
							
							}
						windowView.close();
					}
				});
				windowView.addComponent(actionListBox, LinearLayout.GROWS_HORIZONTALLY);
				windowView.addComponent(back1Btn, LinearLayout.GROWS_HORIZONTALLY);
				guiScreen.showWindow(windowView);

			}
		});
		
		Button modeBtn = new Button("Ustawienia gry", new Action() {

			public void doAction() {
				Window windowView = new Window("Gra");				
				windowView.setWindowSizeOverride(new TerminalSize(100, 50));
				RadioCheckBoxList actionListBox2 = new RadioCheckBoxList();
				
				actionListBox2.addItem("Łatwy");
				actionListBox2.addItem("Średni");
				actionListBox2.addItem("Trudny");
				actionListBox2.addItem("Piekło");
				actionListBox2.setCheckedItemIndex(options[0][1]);
				
				Button back2Btn = new Button("Powrót", new Action() {

					public void doAction() {
						if(actionListBox2.getCheckedItem().equals("Łatwy")) {
							options[0][1] = 0;
							options[1][0] = 10;
							options[1][1] = 5;
							options[1][2] = 200;
							
						}else if(actionListBox2.getCheckedItem().equals("Średni")) {
							options[0][1] = 1;
							options[1][0] = 5;
							options[1][1] = 10;
							options[1][2] = 170;
								
							}else if(actionListBox2.getCheckedItem().equals("Trudny")) {
								options[0][1] = 2;
								options[1][0] = 3;
								options[1][1] = 10;
								options[1][2] = 140;
							
							}else if(actionListBox2.getCheckedItem().equals("Piekło")) {
								options[0][1] = 3;
								options[1][0] = 1;
								options[1][1] = 23;
								options[1][2] = 110;
							
							}
						windowView.close();
					}
				});
				windowView.addComponent(actionListBox2, LinearLayout.GROWS_HORIZONTALLY);
				windowView.addComponent(back2Btn, LinearLayout.GROWS_HORIZONTALLY);
				guiScreen.showWindow(windowView);
			}
		});

		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		window.addComponent(new EmptySpace());
		viewSettingsBtn.setAlignment(Alignment.CENTER);
		window.addComponent(viewSettingsBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(modeBtn, LinearLayout.GROWS_HORIZONTALLY);
		window.addComponent(backBtn, LinearLayout.GROWS_HORIZONTALLY);
		guiScreen.showWindow(window);
		
		return options;
	}

	// ----------------------------------------------------------------------------------------------
	// getters and setters
	// ------------------------------------------------------------------------------------------------

	public Screen getScreen() {
		return gameScreen;
	}

	public void setScreen(Screen screen) {
		this.gameScreen = screen;
	}

	public Terminal getTerminal() {
		return terminal;
	}

	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	public Color getSnakeColor() {
		return snakeColor;
	}

	public void setSnakeColor(Color snakeColor) {
		this.snakeColor = snakeColor;
	}

	public String getSnakeLookBody() {
		return snakeBodyLook;
	}

	public void setSnakeLookBody(String snakeLookBody) {
		this.snakeBodyLook = snakeLookBody;
	}

	public String getSnakeHeadLook() {
		return snakeHeadLook;
	}

	public void setSnakeHeadLook(String snakeHead) {
		this.snakeHeadLook = snakeHead;
	}

	public String getAppleLook() {
		return appleLook;
	}

	public void setAppleLook(String appleLook) {
		this.appleLook = appleLook;
	}

	public String getBorder() {
		return border;
	}

	public void setBorder(String border) {
		this.border = border;
	}

	public Color getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(Color borderColor) {
		this.borderColor = borderColor;
	}

	public Color getAppleColor() {
		return appleColor;
	}

	public void setAppleColor(Color appleColor) {
		this.appleColor = appleColor;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public Key getKey() {
		return key;
	}

	public void setKey() {
		this.key = guiScreen.getScreen().readInput();
	}
}
package com.lukaszborowski.model;


public class Score implements Comparable<Score>{
	
	//----------------------------------------------------------------------------------------------
	//private fields
	//------------------------------------------------------------------------------------------------

	private String userName;
	private int score;
	private long gameTime;
	//----------------------------------------------------------------------------------------------
	//constructor
	//------------------------------------------------------------------------------------------------

	
	public Score(String userName, int score, long gameTime) {
		this.userName = userName;
		this.score = score;
		this.gameTime = gameTime;
	}
	
	//----------------------------------------------------------------------------------------------
	//methods
	//------------------------------------------------------------------------------------------------

	@Override
	public int compareTo(Score o) {
		if(this.gameTime > o.gameTime) {
			return 1;
		}else {
			return -1;
		}
	}
	
	@Override
	public String toString() {

		return userName + ". Wynik: " + Integer.toString(score) + ". ";
	}
	
	//----------------------------------------------------------------------------------------------
	//getters and setters
	//------------------------------------------------------------------------------------------------

	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}

	public long getGameTime() {
		return gameTime;
	}

	public void setGameTime(long gameTime) {
		this.gameTime = gameTime;
	}



}

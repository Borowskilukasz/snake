package com.lukaszborowski.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.lukaszborowski.model.Direction;
import com.lukaszborowski.model.Position;
import com.lukaszborowski.model.Score;
import com.lukaszborowski.model.Snake;
import com.lukaszborowski.view.CurrentView;
import com.lukaszborowski.view.FXView;



public class GameController implements Runnable{

	//----------------------------------------------------------------------------------------------
	//private fields
	//------------------------------------------------------------------------------------------------

	private boolean isStarted = false;
	private CurrentView view;
	private DirectionController directionController = new DirectionController();
	private ScoreController scoreController = new ScoreController();
	private int gameHeight;
	private int gameLenght;
	private String key;
	private Snake snake; 
	private String snakeName = "Go��";
	boolean cleanTable = false;
	private Position apple;
	private AppleController appleController;
	private ArrayList<Score> scoreList = new ArrayList<>();
	private long whenStartedTime = 0;
	private String[] controllKeys = {
			"ArrowUp",
			"ArrowDown",
			"ArrowLeft",
			"ArrowRight"
	};
	private int[][] gameOptions = {{0,0},{5,10,200}};
	
	//----------------------------------------------------------------------------------------------
	//constructor
	//------------------------------------------------------------------------------------------------


	public GameController(CurrentView view, int gameLenght, int gameHeight) {
		this.view = view;
		this.gameLenght = gameLenght;
		this.gameHeight = gameHeight;
	}
	
	
	//----------------------------------------------------------------------------------------------
	//methods
	//------------------------------------------------------------------------------------------------

	//----------------------------------------------------------------------------------------------
	//metody obslugujace widoki
	//------------------------------------------------------------------------------------------------

	/**
	 * Metoda odpowiada za wy�wietlanie danych na ekranie tu� po uruchomieniu programu,
	 * oraz wy�wietla odpowiedni widok po wybraniu odpowiedniej opcji
	 * 
	 * @see CurrentView#showFirstScreen(String)
	 * @see CurrentView#changeSnakeNameScreen(String)
	 * @see CurrentView#showBestResultsView(String[], int[])
	 * @see CurrentView#showSettingList(int[][])
	 * @see CurrentView#showAbout()
	 */
	public void firstScreen() {
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: GameController.firstscreen");
		if(FXView.DEBUG_NAME) System.out.println("NAME: GameController.firstscreen- wynik metody view.showFirstScreen(snakeName): " + view.showFirstScreen(snakeName));
		if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS:1 GameController.firstscreen- wynik metody view.showSettingList(gameOptions)" + gameOptions[0][0]);
		if(FXView.DEBUG_SETTINGS_GAME) System.out.println("SETTINGS2:1 GameController.firstscreen- wynik metody view.showSettingList(gameOptions)" + gameOptions[0][1]);
		
		//while(true) {
		//	int choice;
		switch(view.showFirstScreen(snakeName)) {
		case 1:
			if(FXView.DEBUG_GAME) 	System.out.println("GAME: STARTING GAME!");			
			game();
			break;
		case 2:
			snakeName = view.changeSnakeNameScreen(snakeName);
			break;
		case 3:
			scoreList = scoreController.readScoresFromFile();
			String[] names = new String[scoreList.size()];
			int[] results = new int[scoreList.size()];
			for(int i = 0 ; i < scoreList.size() ; i++) {
				names[i] = scoreList.get(i).getUserName();
				results[i] = scoreList.get(i).getScore();
			}
			view.showBestResultsView(names, results);
			break;
		case 4:

			if(FXView.DEBUG_SETTINGS_GAME) System.out.println("SETTINGS2:2 GameController.firstscreen- wynik metody view.showSettingList(gameOptions)" + gameOptions[0][1]);
			if(FXView.DEBUG_SETTINGS_VIEW) System.out.println("SETTINGS:2 GameController.firstscreen- wynik metody view.showSettingList(gameOptions)" + gameOptions[0][0]);
			gameOptions = view.showSettingList(gameOptions);
			break;
		case 5:
			view.showAbout();
			break;
			
		}
		}
	//}
	
	private void startNewGame() {
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: GameController.startNewGame");
		this.snake = new Snake(30, 10, 7, Direction.RIGHT);	
		this.snake.setName(snakeName);
		this.snake.setPauseTime(gameOptions[1][2]);
		appleController = new AppleController(snake, gameLenght, gameHeight);
		this.apple = appleController.generateApple();
		isStarted = true;
		whenStartedTime = System.currentTimeMillis();
		directionController.setKeys(controllKeys);
	}
	
	
	//----------------------------------------------------------------------------------------------
	//metoda obslugujaca gre
	//------------------------------------------------------------------------------------------------

	/**
	 * Metoda w pierwszej kolejno�ci sprawdza czy zosta�a rozpocz�ta rozgrywka,
	 * po czym pobiera z obiektu apple wsp�rz�dne x i y, tworzy pierwszy widok {@link #gameViewActualization(LinkedList, int, int)},
	 *  tworzy kolejk� przycisk�w do obs�ugi sterowania, 
	 * inicjuje zmienn� przechowuj�c� czas pojawienia sie weza na nowej pozycji, po czym przechodzi do p�tli obs�uguj�cej program.
	 * W p�tli programu pobieramy przycisk od gracza {@link CurrentView#setKey()}, po czym dany przycisk zmieniamy na typ String.
	 * Wywo�ujemy metod� {@link #getKey(LinkedList, String)} z przekazanym w parametrze przyciskiem jako String.
	 * Je�li od zrobienia kroku do aktualnego momentu minelo wiecej czasu niz czas ustalony w {@value #gameOptions} podczas rozpoczecia gry 
	 * lub {@link AppleController#eatApple(int, int, Position)} podczas trwania gry, to sprawdzamy czy glowa weza nie znajduje sie na jablku 
	 * podstawiajac pod {@value #apple} wynik metody {@link AppleController#eatApple(int, int, Position)}.
	 * Sprawdzamy r�wnie� czy nie minal czas zycia jablka r�wnez podstawiajac pod zmienna {@value #apple} wynik metody {@link AppleController#appleLost(long, Position)}.
	 * Po czym aktualizujemy widok {@link #gameViewActualization(LinkedList, int, int)}.
	 * Pobieramy kolejny keirunek {@link #getMove(LinkedList, int, int)}.
	 * Sprawdzamy czy waz nie wszedl na granice planszy, lub na wlasne cialo, jesli tak ustawiamy {@value Snake#setCrashed(boolean)} na {@code true} 
	 * i obs�ugujemy zdarzenie zakonczenia gry ustawiajac {@value #isStarted} na false, {@value Snake#setGameTime(long)}, obliczamy wynik ko�cowy gracza,
	 * tworzymy nowy obiekt Score i przekazujemy go do tablicy wynikow za pomoca {@link ScoreController#addScoreToScoreboard(Score)}, 
	 * ostatecznie uruchamiamy {@link CurrentView#showGameResult(int, String)} i w zale�no�ci od zwr�conego wyniku zamykamy okno gry lub uruchamiamy gr� od nowa. 
	 * 
	 * 
	 */
	public void game() {
		
		if(!isStarted) {
		startNewGame();
		}
		if(FXView.DEBUG_GAME) 	System.out.println("GAME: GameController.game");
		int appleColumn = apple.getCol();
		int appleRow = apple.getRow();

		//inicjalizujemy pierwszy ekran do gry 
		gameViewActualization(snake.getSnakeBody(), appleColumn, appleRow);
		
			// kolejka przyciskow do obsluzenia
			LinkedList<Direction> keyQueue = new LinkedList<>();
			// zmienna przechowujaca czas od postawienia kroku weza
			long stepStartMillis = 0;

			// petla programu
			while (true) {
				view.setKey();					
				key = view.keyToString();					
				//pobieramy przycisk z klawiatury i dodajemy go do kolejki przyciskow do wykonania
				getKey(keyQueue, key);					
				// zmienna zapisująca aktualny czas
				long currentMillis = System.currentTimeMillis();
				// jeśli od zrobienia kroku do teraz minęło więcej niż czas ustalony w zmiennej pauseTime
				if (currentMillis - stepStartMillis > snake.getPauseTime()) {
					stepStartMillis = currentMillis;
					
				 apple = appleController.eatApple(gameOptions[1][0], gameOptions[1][1], apple);				 
				 apple = appleController.appleLost(currentMillis, apple);
				 appleColumn = apple.getCol();
				 appleRow = apple.getRow();
				 gameViewActualization(snake.getSnakeBody(), appleColumn, appleRow);
				 // jeżeli w kolejce są jakieś kierunki to wykonujemy kolejny krok w kierunku z kolejki
					getMove(keyQueue, appleColumn, appleRow);					
					// spradzenie czy waz nie wpadl na granice planszy oraz czy się nie zjadł 
					if(crashCheck(snake.getSnakeBody().getFirst().getCol(), snake.getSnakeBody().getFirst().getRow(), gameLenght, gameHeight) ||
						snakeEatHimself(snake.getSnakeBody())) {
					snake.setCrashed(true);
					}					
					//obsluga zdarzenia zakonczenia gry
					if (snake.isCrashed()) {
						System.out.println("CRASHED!!!!!!!!!!!!!!!!!!");
						isStarted = false;
						snake.setGameTime(System.currentTimeMillis() - whenStartedTime);
						int score =(snake.getAppleEated()*100 - snake.getAppleMissed()*50 + (int)(snake.getGameTime()%10000)/1000);
						Score result = new Score(snake.getName(), score, snake.getGameTime());
						try {
							scoreList = scoreController.addScoreToScoreboard(result);
						} catch (IOException e) {
							e.printStackTrace();
						}
						if( view.showGameResult(score, snake.getName() ) == 1) {
							startNewGame();	
						}else {
								break;					
						}
					}
				}
				continue;
			}
	}
	
	//----------------------------------------------------------------------------------------------
	//metody pomocnicze do gry
	//------------------------------------------------------------------------------------------------

	/**
	 * Metoda aktualizuje widok
	 * @param snakeBody cialo weza przekazane do narysowania 
	 * @param appleColumn wspolzedna polozenia jablka
	 * @param appleRow wspolrzedna polozenia jablka
	 */
	public void gameViewActualization(LinkedList<Position> snakeBody, int appleColumn, int appleRow) {
		view.drawBorders();
		view.makeAppleVisible(appleColumn, appleRow);

		for (int i = 0; i < snakeBody.size() ; i ++) {
			view.drawSnakeElement(snakeBody.get(i).getCol(), snakeBody.get(i).getRow());
		}
		view.drawSnakeHead(snakeBody.get(0).getCol(), snakeBody.get(0).getRow());
		view.refreshScreen();
	}
	
	/**
	 * Metoda sprawdza czy przekazany String z przyciskiem nie jest pusty, je�li nie jest to przy pomocy
	 *  {@link DirectionController#getDirectionFromKey(String)} przypisuje odpowiedni kierunek, po czym 
	 *  sprawdza czy w kolejce sa jakies przyciski, jesli nie sprawdza czy dany kierunek nie jest przeciwny do aktualnego,
	 *  je�li nie- dodaje przycisk do kolejki.
	 *  R�wnie� je�li w kolejce znajduj� si� jakie� kierunki do wykonania, metoda sprawdza czy przekazany kierunek nie jest 
	 *  przeciwny do ostatniego kierunku w kolejce i dodaje go na koniec kolejki.
	 *  
	 *  Metoda obs�uguje r�wnie� sytuacje nacisniecia przyciskow pauzy wywo�uj�c {@link CurrentView#showPauseView(int, String)},
	 *  z obliczonym wynikiem do wyswietlenia.
	 *  W zale�no�ci od zwr�conego inta zostaje wykonana odpowiednia akcja:
	 *  <ul>
	 *  	<li>powrot do gry i ustawienie odpowiedniego czasu dla jablka </li>
	 *  	<li>Rozpoczecie gry od nowa </li>
	 *  	<li>Zako�czenie gry i wy�wietlenie okna glownego</li>
	 *  </ul>
	 *  
	 * @param keyQueue kolejka przyciskow zamienionych na odpowiednie kierunki
	 * @param key aktualny przycisk ktory chcemy dodac do kolejki
	 */
	public void getKey(LinkedList<Direction> keyQueue, String key) {
		// zmiennna przechowujaca nacisniety przycisk
		Direction direction = null;

		if (!(key.equals(""))) {
			// jesli jest nacisniety jakis przycisk funkcja getDirectionFromKey sprawdza czy
			// jest to odpowiedni przycisk
			// i jesli tak to zwraca odpowiedni kierunek
			direction = directionController.getDirectionFromKey(key);
			// jesli przycisk zgadza sie z sterowaniem
			if (direction != null) {
				// jesli w kolejce nie ma zadnego innego przycisku
				if (keyQueue.size() == 0) {
					// i jesli podany kierunek przez uzytkownika nie jest kierunkiem przeciwnym do
					// aktualnego
					if (snake.getDirection() != directionController.getOppositeDirection(direction)) {
						// to dodajemy kierunek do kolejki wykonania
						keyQueue.add(direction);
					}
					// w przypadku rowniez gdy w kolejce jest jakis kierunek dodajemy do kolejki
					// aktualny kierunek pod warunkiem ze nie jest to kierunek przeciwny do
					// ostaniego kierunku w kolejce
				} else if (keyQueue.getLast() != directionController.getOppositeDirection(direction)) {
					keyQueue.add(direction);
				}
			}
			if(key.equals("Escape") || key.equals("NormalKey:  ")) {
				long whenPauseButtonHitted = System.currentTimeMillis();
				int score =(snake.getAppleEated()*100 - snake.getAppleMissed()*50 + (int)(snake.getGameTime()%20000)/1000);
				int choice = view.showPauseView(score, snakeName);
				switch(choice) {
				case 0:
					apple.setTimeOfCreate(apple.getTimeOfCreate() + (System.currentTimeMillis() -whenPauseButtonHitted) );
					break;
				case 1:
					startNewGame();
					break;
				case 2:
					firstScreen();
					break;
				}
			}
		}
	}
	
	/**
	 * Metoda zmienia odpowiednio wspolrzedne polozenia wszystkich elementow weza
	 * @param direct kierunek w ktory ma sie skierowac waz podczas zmiany polozenia
	 */
	public void makeStep(Direction direct) {
        if (snake.isCrashed()) {
            return;
        }
        snake.setDirection(direct);

        Position newBodyPart = null;

        if (snake.isEat()) {
            newBodyPart = new Position(snake.getSnakeBody().getLast().getCol(), snake.getSnakeBody().getLast().getRow());
            snake.setAppleEated();
        }

        for (int i = snake.getSnakeBody().size() - 1; i > 0; i--) {
        	snake.getSnakeBody().get(i).setCol(snake.getSnakeBody().get(i - 1).getCol());
        	snake.getSnakeBody().get(i).setRow(snake.getSnakeBody().get(i - 1).getRow());
        }
        if (snake.getDirection()== Direction.LEFT) {
        	snake.getSnakeBody().get(0).setCol(snake.getSnakeBody().get(0).getCol() - 1);
        }
        if (snake.getDirection() == Direction.RIGHT) {
        	snake.getSnakeBody().get(0).setCol(snake.getSnakeBody().get(0).getCol() + 1);
        }
        if (snake.getDirection() == Direction.UP) {
        	snake.getSnakeBody().get(0).setRow(snake.getSnakeBody().get(0).getRow() - 1);
        }
        if (snake.getDirection() == Direction.DOWN) {
        	snake.getSnakeBody().get(0).setRow(snake.getSnakeBody().get(0).getRow() + 1);
        }
        
        if (snake.isEat()) {
        	snake.getSnakeBody().add(newBodyPart);
            snake.setEat(false);
        }
    }
	
	/**
	 * Metoda zmienia odpowiednio wspolrzedne polozenia wszystkich elementow weza gdy kierunek nie zostaje zmieniony
	 */
	public void makeStep() {
		makeStep(snake.getDirection());
	}
	
	/**
	 * Metoda sprawdza czy w kolejce sa jakies przyciski, jesli sa, wykonuje {@link #makeStep(Direction)} 
	 * po czym aktualizuje widok z wezem w nowym polozeniu za pomoca metody {@link #gameViewActualization(LinkedList, int, int)}.
	 * Jesli nie ma zadnych kierunkow w kolejce wywolywana zostaje metoda {@link #makeStep()} i analogicznie zostaje 
	 * zaktualizowany widok z wezem w nowej pozycji za pomoca metody {@link #gameViewActualization(LinkedList, int, int)}.
	 * @param keyQueue kolejka przyciskow do wykonania
	 * @param appleColumn wspolrzedna pozioma jablka 
	 * @param appleRow wspolrzedna pionowa jablka
	 */
	public void getMove(LinkedList<Direction> keyQueue, int appleColumn, int appleRow) {
		if (keyQueue.size() > 0) {
			// wykonujemy krok w kierunku z początku kolejki i wykonujemy krok
			makeStep(keyQueue.removeFirst());
			gameViewActualization(snake.getSnakeBody(), appleColumn, appleRow);

		} else {
			// jeśli nie było żadnego nowego kierunku w kolejce waz robi krok w tym kierunku
			// ktory byl zapisany jako ostatni
			makeStep();
			gameViewActualization(snake.getSnakeBody(), appleColumn, appleRow);
		}
	}
	
	/**
	 * Metoda sprawdza czy waz nie najechal� glowa na pole gdzie znajduje sie aktualnie jego cialo
	 * @param snake aktualny waz
	 */
	public boolean snakeEatHimself(LinkedList<Position> snakeBody) {
		for (int i = 1; i < snakeBody.size() ; i++) {
			if(snakeBody.get(i).equals(snakeBody.getFirst())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Metod sprawdza czy waz nie wpadl na krawedz pola gry
	 * @param snakeHead
	 * @param gameLenght
	 * @param gameHeight
	 * @return
	 */
	public boolean crashCheck(int snakeHeadColumn, int snakeHeadRow, int gameLenght, int gameHeight) {
		if (snakeHeadColumn == 0 || 
				snakeHeadRow == 0 || 
					snakeHeadColumn == (gameLenght-1) || 
							snakeHeadRow == (gameHeight-1)) {
			return true;
		}
		return false;
	}
	
	//----------------------------------------------------------------------------------------------
	//getters and setters
	//------------------------------------------------------------------------------------------------

	public String getSnakeName() {
		return snakeName;
	}

	public void setSnakeName(String snakeName) {
		this.snakeName = snakeName;
	}
	
	public int getGameHeight() {
		return gameHeight;
	}
	public void setGameHeight(int gameHeight) {
		this.gameHeight = gameHeight;
	}
	public int getGameLenght() {
		return gameLenght;
	}
	public void setGameLenght(int gameLenght) {
		this.gameLenght = gameLenght;
	}

	public String[] getControllKeys() {
		return controllKeys;
	}

	public void setControllKeys(String[] controllKeys) {
		this.controllKeys = controllKeys;
	}
	

	public int getGameOptions(int x, int y) {
		return gameOptions[x][y];
	}


	public void setGameOptions(int[][] gameOptions) {
		this.gameOptions = gameOptions;
	}


	@Override
	public void run() {
		game();
	}

}

package com.lukaszborowski.controller;

import java.util.Comparator;

import com.lukaszborowski.model.Score;
/**
 * Klasa por�wnuje wyniki graczy,
 *  jesli 2 graczy ma taki sam wynik, wyzej w tabeli jest ten ktory dluzej utrzymal weza przy zyciu
 *  @see Score#compareTo(Score)
 * @author Lukasz
 *
 */
public class ComparatorScore implements Comparator<Score> {


	public int compare(Score o1, Score o2) {
		int score = o2.getScore() - o1.getScore();
		if(score == 0) {
			return o2.compareTo(o1);
		}
		return score;
	}

}

package com.lukaszborowski.controller;

import java.util.Random;

import com.lukaszborowski.model.Position;
import com.lukaszborowski.model.Snake;

/**
 * Klasa AppleContoller posiada metody obsugujace dzia�ania zwi�zane z jablkiem tj.
 * <ul>
 * <li>generowanie jablka</li>
 * <li>usuwanie jablka i generowanie nowego gdy minie pewien czas a jablko nie zostalo zjedzone przez weza</li>
 * <li>usuwanie jablka i generowanie nowego gdy glowa weza znajdzie sie w na tej samej pozycji co jablko</li>
 * <li>sprawdzenie czy jablko znajduje sie na granicy lub na polach na ktorych aktualnie znajduje sie waz</li>
 *</ul>
 * @see #generateApple()
 * @see #appleLost(long, Position)
 * @see #eatApple(int, int, Position)
 * @see #isOnBorderOrSnake(int, int)
 */
public class AppleController {

	private Snake snake;
	private int gameLenght, gameHeight;
	
	public AppleController(Snake snake, int gameLenght, int gameHeight){

		this.setSnake(snake);
		this.setGameLenght(gameLenght);
		this.setGameHeight(gameHeight);
		
	}

	/**
	 * Metoda sluzy do generowania nowego jablka,
	 * sprawdza czy nowe wspolrzedne nie znajduja sie na granicy planszy lub na polach gdzie akutalnie znajduje sie waz,
	 * ustawia takze czasu utworzenia jablka jako aktualny czas
	 * @return obiekt Position ktory zawiera wsp�rz�dne nie kolidujace z wezem oraz granicami planszy w danym momencie
	 */
	public Position generateApple() {
		Random random = new Random();
		int x, y;

		do {
			 x = random.nextInt(gameLenght);
			 y = random.nextInt(gameHeight);
		} while (isOnBorderOrSnake(x, y));

		Position apple = new Position(x,y);
		apple.setTimeOfCreate(System.currentTimeMillis());
		return apple;
	}

	/**
	 * Metoda obsluguje sytuacje gdy minie pewien czas od utworzenia jablka a waz go nie zje
	 * Do zmiennej appleMissed snake'a zostaje dodany 1 punkt ktory jest brany pod uwage podczas obliczania wyniku gracza na koniec gry
	 * @param currentMillis aktualny czas
	 * @param apple obiekt jablka ktory posiada
	 * @return nowa pozycje jablka na planszy
	 */
	public Position appleLost(long currentMillis, Position apple) {
		//petla obslugujaca znikanie jablka po okreslonym czasie 
		if(( currentMillis - apple.getTimeOfCreate()) > (snake.getPauseTime()*gameLenght + 6000)) {
			 System.out.println(Long.toString(currentMillis - apple.getTimeOfCreate()));
			snake.setMissed(true);
			snake.setAppleMissed();
			return generateApple();
		}
		return apple;
	}

	
	/**
	 * Metoda jest odpowiedzialna za sprawdzenie czy g�owa weza znajduje sie na tej samej pozycji co jablko,
	 * jesli tak, ustawiamy zmienn� eat w obiekcie Snake na true i zwi�kszamy pr�dko�� w�a zgodnie z wybranym trybem gry
	 * @param frequency_speed_change zmenna m�wi o tym po ilu zjedzonych jab�kach w�� ma przyspieszy�
	 * @param mode zmienna m�wi o tym o ile ms ma by� kr�tsza przerwa mi�dzy krokami w�a gdy w�� przyspiesza
	 * @param apple obiekt typu Position m�wi�cy o aktualnych wsp�rz�dnych jab�ka
	 * @return nowy obiekt typu Position z nowymi wsp�rz�dnymi jab�ka 
	 */
	public Position eatApple(int frequency_speed_change, int mode, Position apple) {
		Position newApple = apple;
		// obsluga zdarzenia gdy głowa weza najedzie na jablko
		if (snake.getSnakeBody().getFirst().equals(apple)) {
			snake.setEat(true);
			//zwiekszenie predkosci weza zgodnie z ustawieniami
			
			if(snake.getPauseTime() >= 42 && snake.getPauseTime() != 50 && snake.getAppleEated()%frequency_speed_change == 0) {
				snake.setPauseTime(snake.getPauseTime()- mode); 
				System.out.println(snake.getPauseTime());
			}
			
			newApple = generateApple();
			
		}
		return newApple;
	}

/**
 * Metoda sluzy do sprawdzenia czy przekazane jako parametry wsp�rz�dne znajduj� sie na granicy planszy lub 
 * na polach na ktorych aktualnie znajduje sie waz
 * @param x wspolrzedna pozioma 
 * @param y wspolrzedna pionowa
 * @return @code{true} jesli dane wspolrzedne pokrywaja sie z wspolrzednymi weza lub granicy @code{false} jesli na danych wspolrzednych nic nie ma 
 */
	public boolean isOnBorderOrSnake(int x, int y) {
		Position position = new Position(x, y);
				if (position.getCol() == 0 || position.getRow() == 0 || position.getCol() == (gameLenght - 1)
				|| position.getRow() == (gameHeight - 1)) {
			return true;
		}
		if (snake.getSnakeBody().contains(position)) {
			return true;
		}

		return false;
	}

	public Snake getSnake() {
		return snake;
	}

	public void setSnake(Snake snake) {
		this.snake = snake;
	}

	public int getGameLenght() {
		return gameLenght;
	}
	public void setGameLenght(int gameLenght) {
		this.gameLenght = gameLenght;
	}
	public int getGameHeight() {
		return gameHeight;
	}
	public void setGameHeight(int gameHeight) {
		this.gameHeight = gameHeight;
	}


}

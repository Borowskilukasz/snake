package com.lukaszborowski.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import com.lukaszborowski.model.Score;

/**
 * Klasa odpowiedzialna jest za operacje wykonywane na wyniku 
 * @author Lukasz Borowski
 *
 */
public class ScoreController {

	private final String FileName = "ScoreBoard";

	/**
	 * Metoda odpowiada za odczyt tabeli wynik�w z pliku 
	 * @return ArrayList z maksymalnie 10 wynikami zapisanymi jako obiekty typu Score
	 */
	public ArrayList<Score> readScoresFromFile() {

		ArrayList<Score> scoreboard = new ArrayList<>();
		long time = -1;
		int score = -1;
		String name = "";
		Score actualScore = null;
		Scanner inputFile = null;
		File file = new File(FileName);
		if (file.isFile()) {
			try {
				inputFile = new Scanner(new BufferedReader(new FileReader(FileName)));

				while (inputFile.hasNext()) {
					if (inputFile.hasNextInt()) {
						score = inputFile.nextInt();
						time = inputFile.nextLong();
					} else {
						name = inputFile.next();
					}
					
					if (!name.equals("") && score != -1) {
						actualScore = new Score(name, score, time);
						scoreboard.add(actualScore);	
						name = "";
						score = -1;
						time = -1;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (inputFile != null) {
					inputFile.close();
				}
			}
		}
		
		return scoreboard;
	}

	/**
	 * Metoda odpowiedzialna jest za dodawanie wyniku do pliku z wynikami.
	 * Je�li plik nie zawiera zadnych wynikow, to dodajemy wynik, 
	 * je�li z odczytanego pliku pobrali�my mniej wynik�w ni� 10 to dodajemy wynik do tabeli po czym segregujemy j�, 
	 * je�li pobrali�my 10 wynik�w, sprawdzamy czy aktualny wynik jest wi�kszy od ostatniego wyniku w tabeli. 
	 * Je�li tak, dodajemy wynik do tabeli, segregujemy tabel�i usuwamy ostatni wynik aby w tabeli nie znajdowa�o si� wiecej wynik�w ni� 10
	 * Po wszystkim nadpisujemy plik z wynikami now� tabel�
	 * @see ComparatorScore
	 * @param score aktualny wynik zdobyty przez gracza
	 * @return nowa tabela z dodanym wynikiem gracza
	 * @throws IOException metoda mo�e rzuci� wyj�tek odczytujemy tabele z pliku {@link ScoreController#readScoresFromFile()}
	 */
	public ArrayList<Score> addScoreToScoreboard(Score score) throws IOException{
		ArrayList<Score> scoreboard = readScoresFromFile();

		if(scoreboard.size() == 0) {
			scoreboard.add(score);			
		}else if (scoreboard.size() < 10) {
			scoreboard.add(score);
			Collections.sort(scoreboard, new ComparatorScore());
		}else {

			if( score.getScore() > scoreboard.get( scoreboard.size()-1 ).getScore() ) {
				scoreboard.add(score);
				Collections.sort(scoreboard, new ComparatorScore());
				scoreboard.remove(10);	
			}
		}
			FileWriter fileWriter = new FileWriter(FileName);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);				
			  
			try {
				   for (Score scores: scoreboard) {
				      bufferedWriter.write(scores.getUserName() + " " + scores.getScore()  + " " + scores.getGameTime());
				      bufferedWriter.newLine();
					}
				  
			    }catch (IOException ex) {
					ex.printStackTrace();
				} finally {
					bufferedWriter.close();
					fileWriter.close();
			  }
		return scoreboard;

	}
}

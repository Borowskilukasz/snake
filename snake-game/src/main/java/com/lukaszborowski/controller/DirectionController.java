package com.lukaszborowski.controller;

import com.lukaszborowski.model.Direction;

/**
 * Dana klasa odpowiada za obsluge zdarzen zwiazanych z kierunkiem tj
 * <ul>
 * 	<li>dopasowanie odpowiedniego kierunku do klawiszy</li>
 * <li>zwrocenie przeciwnego kierunku</li>
 * </ul>
 * @author Lukasz Borowski
 *
 */
public class DirectionController {

	private String[] keys;
	
	public DirectionController() {}

	/**
	 * Metoda sprawdza czy zosta� wci�ni�ty kt�ry� z przycisk�w steruj�cych,
	 * je�li tak, zwraca odpowiedni kierunek dla przycisku
	 * @param key wci�ni�ty przycisk przez gracza przekazany jako String
	 * @return odpowiedni obiekt Direction dla odpowiedniego przycisku, jesli �aden z wci�ni�tych przycisk�w nie pasuje @code{null}
	 */
	public Direction getDirectionFromKey(String key) {
		if (key.equals(keys[0])) {
			return Direction.UP;
		} else if (key.equals(keys[1])) {
			return Direction.DOWN;
		} else if (key.equals(keys[2])) {
			return Direction.LEFT;
		} else if (key.equals(keys[3])) {
			return Direction.RIGHT;
		} else {
			return null;
		}
	}

	/**
	 * ta metoda sprawdza jaki jest aktualny kierunek i zwraca kierunek przeciwny
	 * zapobiega ona sytuacji gdy ktos przypadkowo nacisnie przycisk odwrotny do kierunku weza 
	 * @see GameController#getKey(java.util.LinkedList, String)
	 * @param direction aktualny kierunek w ktorym sie porusza waz
	 * @return przeciwny kierunek w ktorym sie porusza waz
	 */
	public Direction getOppositeDirection(Direction direction) {
		if (direction == Direction.LEFT)
			return Direction.RIGHT;
		if (direction == Direction.RIGHT)
			return Direction.LEFT;
		if (direction == Direction.UP)
			return Direction.DOWN;
		if (direction == Direction.DOWN)
			return Direction.UP;
		throw new IllegalArgumentException(
				"Something went terribly wrong in getOppositeDirection function. Passed direction: " + direction);
	}

	public String[] getKeys() {
		return keys;
	}

	public void setKeys(String[] keys) {
		this.keys = keys;
	}
	
}
